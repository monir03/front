<?php
// session_start();
// include_once '../../vendor/autoload.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Log in</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="css/style.css" type="text/css"/>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <style>
    </style>
</head>
<body>
<!--   action="loginUser.php-->
   <div class="container">
   <div class="row">
   
   
   <div class="col-md-6 col-sm-4 wrapper">
      <div class="jumbotron">
       
        <h2>
           Manage your residence smartly !!
       </h2>
      <img src="images/buildings.png" alt="" style="width:300px;height:300px">
      </div>   
   </div>
   
   <div class="col-md-6 wrapper">
       <div class="panel panel-login">
           <div class="panel-heading">
               <div class="row">
                   <div class="col-xs-6">
                       <a href="#loginform" class="active" id="login-form-link">Sign in</a>
                   </div>
                    <div class="col-xs-6">
                       <a href="#registerform" class="active" id="register-form-link">Sign up</a>
                   </div>
               </div>
               <hr/>
           </div>
           
           <div class="panel-body">
               <div class="row">
                   <div class="col-lg-12">
                       <?php
                           // if(\App\Session::get('msg')){
                               // echo "<div class='alert colorOrange'>".\App\Session::get('msg')."</div>";
                               // session_unset();
                           // }
                        ?>

                        <form class="form-signin" method="post"  id="loginform" action="AdminLogin.php">
<!--                          <h2 class="form-signin-heading">Sign in</h2>-->

                                    <span id="emailcheck"></span>
                                    <label for="email">Email</label>
                                    <input type="text" class="form-control" name="email" id="email" placeholder="Email Address"  autofocus="" />

                                    <span id="passwordcheck"></span>
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control" name="password" id="password" placeholder="Password"/>
                                    <button class="btn btn-lg btn-login btn-block button" type="submit" name="login">Login</button>
                                   <br/> 
                                   <div class="info">If you are new please Sign up first!</div>
                        </form>



                        <form id="registerform" action="" method="post" role="form" style="display: none;" class="register-form" enctype="multipart/form-data">
                            <h2>Admin registration</h2>
                            <div class="form-group">
                            <span id="nameValid" style="color:red;font-weight:bold"></span>
                            <label for="name" id="label1">Enter your name:</label>
                            <input type="text" class="form-control" id="name1" placeholder="Enter name" name="name" >
                            </div>
                            <div class="form-group">
                               <label for="email" id="label2">Enter your email:</label>
                                <input type="email" name="email" id="email1" tabindex="1" class="form-control" placeholder="Email" value="">
                            </div>

                            <div class="form-group">
                                <label for="mobile" id="label4">Enter your mobile:</label>
                                <input type="text" class="form-control" id="mobile" placeholder="Mobile" name="mobile">
                            </div>
                            

                              <div class="form-group">
                                   <table>
                                       <tr>
                                           <td>

                                           <label for="image" id="label6">Your image:</label>
                                           <span id="checkImageSize" class="label label-danger"></span>
                                           <input type="file" class="form-control" id="image"  name="image"></td>
                                       </tr>
                                   </table>
                                </div>


                            <div class="form-group">
                               <label for="password1" id="label7">Enter password</label>
                               <span class="label label-danger" id="wrongpassword"></span>
                                <input type="password" name="password1" id="password1"  class="form-control" placeholder="Password">
                            </div>



                            <div class="form-group">
                               <label for="confirmpassword" id="label8">Confirm password</label>
                               <span id="confirmpasswordWarning"></span>

                                <input type="password" name="confirmpassword" id="confirmpassword" tabindex="2" class="form-control" placeholder="Confirm Password">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <button type="button" name="registersubmit" id="registersubmit" tabindex="4" class="form-control btn btn-register">Register Now</button>
                                         
                                    </div>
<!--                                    <button type="reset"  class="form-control btn btn-register" name="Reset">Reset</button>-->
                                </div>
                            </div>
<!--                            <span id="result"></span>-->
				        </form>
                   </div>
               </div>
           </div>
       </div>
        
        <div id="dialog-message" title="Registration complete" style="display:none">Registration complete !! go to <i
            
         class="fa fa-sign-in" aria-hidden="true"></i> <a href="login.php">Login</a></div>
        
        <div id="dialog-message1" title="Registration failed!!" style="display:none;color:red">Email already exist ! Try another</div>
   </div>
</div>
</div>
 
 
  
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
      $(document).ready(function() {
        var mailformat = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        $('#login-form').on('click','.button',function(e){
            var email = $('#email').val();
			var password = $('#password').val();
             
             if(email=='' || (!mailformat.test(email))){
                document.getElementById("emailcheck").innerHTML="<div class='alert danger'><center>Fill up each field properly!!</center></div>"; 
                $('#email').css('box-shadow','0 0 10px ##ff0000');
                $('#email').css('border-color','#ff0000');
                 e.preventDefault();
            }
            if(password==''){
                 document.getElementById("emailcheck").innerHTML="<div class='alert danger'><center>Fill up each field properly!!</center></div>"; 
                $('#emailcheck').delay(100).fadeIn(100);
                $('#emailcheck').fadeOut(2000);
                $('#password').css('box-shadow','0 0 10px ##ff0000');
                $('#password').css('border-color','#ff0000');
                e.preventDefault();
            }
             
         });
          
          
            $('#email').keyup(function(){
              var $this = $(this);
              var insertedVal = $this.val();
              if (insertedVal !=''){
                $('#email').css('box-shadow','0 0 10px green');
                 $this.css({"color":"green","border":"1px solid green"});
              }else{
                  $('#email').css('box-shadow','0 0 10px #ff0000');
                 $this.css({"color":"green","border":"1px solid #ff0000"});
              }
           });
          
            $('#password').keyup(function(){
              var $this = $(this);
              var insertedVal = $this.val();
              if (insertedVal !='' ){
                $('#password').css('box-shadow','0 0 10px green');
                 $this.css({"color":"green","border":"1px solid green"});
              }else{
                  $('#password').css('box-shadow','0 0 10px #ff0000');
                 $this.css({"color":"green","border":"1px solid #ff0000"});
              }
           });
          
          
          
          //Check input value for registration form//
          //----------------------------------------------------------------------------------------------------------------------//
             $('input[type=text],#confirmpassword').keyup(function(){
              var $this = $(this);
              var insertedVal = $this.val();
              if (insertedVal !=''){
                $this.css('box-shadow','0 0 10px green');
                $this.css({"color":"green","border":"1px solid green"});
              }else{
                $this.css('box-shadow','0 0 10px #ff0000');
                $this.css({"color":"red","border":"1px solid #ff0000"})
              }
           });
          
          $('#email1').keyup(function(){
              var $this = $(this);
              var insertedVal = $this.val();
              if (insertedVal !='' && (mailformat.test(insertedVal))){
                $this.css('box-shadow','0 0 10px green');
                $this.css({"color":"green","border":"1px solid green"});
              }else{
                $this.css('box-shadow','0 0 10px #ff0000');
                $this.css({"color":"red","border":"1px solid #ff0000"});
              }
           });
          
          
    
          //check password length and if it is empty//------------------------------------------------------------------//
          $('#password1').keyup(function(){
              var $this = $(this);
              var insertedVal = $this.val();
              if (insertedVal !='' && insertedVal.length>=10){
                $this.css('box-shadow','0 0 10px green');
                $this.css({"color":"green","border":"1px solid green"});
                $('#wrongpassword').hide();
              }else{
                $('#wrongpassword').show();
                $('#wrongpassword').text('Password must be 10 characters long !!');
                $this.css('box-shadow','0 0 10px #ff0000');
                $this.css({"color":"#ff0000","border":"1px solid #ff0000"})
              }
           });
          
          //Check if password matches................................................................................
          $('#confirmpassword').keyup(function(){
              var p1 = $('#password1').val();
              var p2 = $('#confirmpassword').val();
        
              if(p2==p1 && p2!=''){
                $('#confirmpasswordWarning').html("<i class='fa fa-check' aria-hidden='true'></i>Password  matches !");
                $('#confirmpasswordWarning').css("color","green");
                $('#confirmpassword').css({"color":"green","box-shadow":'0 0 10px green'});
                $('#password1').css('box-shadow','0 0 10px green');
              }
              else{
                $('#confirmpasswordWarning').html(" <i class='fa fa-times' aria-hidden='true'></i>Password doesn't match !");
                $('#confirmpasswordWarning').css({"color": "#ff0000"});
                $('#confirmpassword').css({"color": "#ff0000","border-color":"#ff0000","box-shadow":"0 0 10px #ff0000"});
              }
           });
          

          
          //Check image file size and file type.....................................................................
            var myFile = document.getElementById('image');
            myFile.addEventListener('change', function() {
                var file = this.files[0];
                var fileType = file["type"];
                var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
                if((this.files[0].size)>50000){
                    $('#checkImageSize').show();
                    $('#checkImageSize').text("50 kb is allowed !");
                    $('#image').css({"border-color":"#ff0000","box-shadow":'0 0 10px #ff0000'});
                    document.getElementById('image').value="";
                }else if($.inArray(fileType, ValidImageTypes) < 0){
                    $('#checkImageSize').show();
                    $('#checkImageSize').text("File format is not supported !");
                     $('#image').css({"border-color":"#ff0000","box-shadow":'0 0 10px #ff0000'});
                    document.getElementById('image').value="";
                }
                else{
                    $('#checkImageSize').hide();
                    $('#image').css({"border-color":"green","box-shadow":'0 0 10px green'});
                }

            });
          
          
          //Check if any filed is empty................................................................................................
            $('#registersubmit').click(function(){ 
                
            var name=$('#name1').val();
            var email = $('#email1').val();
            var address = $('#address').val();
            var mobile = $('#mobile').val();

                
            var genValue=false;


            var image = $('#image').val();
            var password = $('#password1').val();
            var confirmpassword = $('#confirmpassword').val();
                
            if(name==''){
                $('#name1').css('border-color','#ff0000');
            }
            if (email=='' || (!mailformat.test(email))){
                $('#email1').css('border-color','#ff0000');
            }

            if(address==''){
                $('#address').css('border-color','#ff0000');
            }

            if(mobile==''){
                $('#mobile').css('border-color','#ff0000');
            }
                

            if(image==''){
                $('#image').css('border-color','#ff0000');
            }else{
                 $('#image').css('border-color','green');
            }

            if(password=='' || password.length<10){
                $('#password1').css('border-color','#ff0000');
            }


            if(confirmpassword==''){
                $('#confirmpassword').css('border-color','#ff0000');
            }
     
            if(name!='' && email!='' && (mailformat.test(email)) && mobile!='' && image!='' && password!='' && password==confirmpassword)
                {
                    var formdata = new FormData($('#registerform')[0]);
                    
                    $.ajax({
					type: "POST",
                    data:formdata,
                    url: "AdminRegistration.php",
                    cache: false,
                    contentType:false,
                    processData:false,

					success: function(responseText){
                        if(responseText==1){
                             $("#dialog-message" ).dialog({
                                  modal: true,
                                  buttons: {
                                    Ok: function() {
                                      $( this ).dialog( "close" );
                                    }
                                  }
                                });
                        }else {
                            $("#email1").css({"color":"red","border-color":"#ff0000","box-shadow":'0 0 10px #ff0000'});
                            $("#dialog-message1" ).dialog({
                                  modal: true,
                                  buttons: {
                                    Ok: function() {
                                      $( this ).dialog( "close" );
                                    }
                                  }
                                });

                        }

					}
                 
				});
                    
                
                }
        });
      });
   
</script>


<script>
    
    $(function(){
          $('#login-form-link').click(function (e){
                $('#loginform').delay(100).fadeIn(100);
                $('#registerform').fadeOut(100);
                $('#register-form-link').removeClass('active');
                $(this).addClass('active');
                e.preventDefault();
            });
            
            
            $('#register-form-link').click(function (e){
                $('#registerform').delay(100).fadeIn(100);
                $('#loginform').fadeOut(100);
                $('#login-form-link').removeClass('active');
                $(this).addClass('active');
                e.preventDefault();
            });
//        
//        
//           $('#register-submit').click(function (){
//              $('#login-form-link').hide();
//                $(this).show();
////                e.preventDefault();
//            });
//        
        
      });
</script>

</body>
</html>